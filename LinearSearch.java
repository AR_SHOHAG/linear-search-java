import java.util.Scanner;


public class LinearSearch {

	public int linear_search(int a[], int n, int key)
	{
		int i;

		for (i = 0; i < n; i++)
	    {
	      if (a[i] == key)
	      {
	         return i;
	      }
	   }
	   return -1;
	}


	public static void main(String[] args) {

		int n_m, j, key_m;

		Scanner in = new Scanner(System.in);
		System.out.println("Enter the number of elements of array");
		n_m=in.nextInt();
		int a[] = new int[n_m];

		System.out.println("Enter " + n_m + " integers");

		for (j = 0; j < n_m; j++)
		  a[j] = in.nextInt();

		System.out.println("Enter the key elements");
		key_m = in.nextInt();


	    LinearSearch l = new LinearSearch();

	    int pos = l.linear_search(a, n_m, key_m);

	    if(pos >= 0){
	    	System.out.println(key_m + " is present at position " + (pos + 1) + ".");
	    }

	    else
	    	System.out.println(key_m + " is not present in the array.");

	}

}